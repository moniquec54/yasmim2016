<?php
    // carrega o array produtos
    require_once "produtos.inc";
	session_start();
	
	if(!isset($_SESSION['carrinho'])) $_SESSION['carrinho'] = [];//pega o item do carrinho [] e atribui ao primeiro registro vazio do arrey.Pois a o ultimo intem já incluido
	$_SESSION['valor_tot'] = 0; //varifica valor total calculado dos arreys de carrinho
?>
<html>
    <head>
        
        <title>Finalizar</title>
		<meta charset="utf-8">
    </head>
    <body>
        <table border='1'>
            <thead>
                <th>Descrição</th>
                <th>Preço</th>
            </thead>
            <tbody>
                <?php 
                foreach($_SESSION['carrinho'] as $codigo)
                {
					$_SESSION['valor_tot'] += $produtos[$codigo]['valor'];// pega o cod do produto e o valor dele
                    echo "<tr><td>" . $produtos[$codigo]['descricao'] . "</td>"// pega o cod do produto e a descricao
                        . "<td>" . number_format($produtos[$codigo]['valor'], 2, ",", ".") . "</td>";
                }
                ?>
            </tbody>
        </table>
        <a href="pagar.php">Pagar</a>
    </body>
</html>