<?php
    // carrega o array produtos
    require_once "produtos.inc";
	session_start();// inicia seção
	
	if(!isset($_SESSION['carrinho'])) $_SESSION['carrinho'] = []; //testa se existe seção se não esxite ele cria um arrey de sessção
?>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Meu carrinho</title>
    </head>
    <body>
        <table border='1'>
            <thead>
                <th>Descrição</th>
                <th>Preço</th>
                <th>Ação</th>
            </thead>
            <tbody>
                <?php 
                foreach($produtos as $codigo => $produto)
                {
                    echo "<tr><td>" . $produto['descricao'] . "</td>"
                        . "<td>" . number_format($produto['valor'], 2, ",", ".") . "</td>"
                        . "<td><form action='adicionar.php' method='POST'>"
                        . "<input type='hidden' name='codigo' value='" . $codigo . "'/>"
                        . "<input type='submit' value='Adicionar'/></form></td></tr>";
                }
                ?>
            </tbody>
        </table>
        <a href="finaliza.php">Finalizar</a>
    </body>
</html>