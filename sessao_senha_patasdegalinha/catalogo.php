<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Catalogo</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<link href="css/css.css" rel="stylesheet" type="text/css">
<link type="text/css" rel="stylesheet" href="css/bootstrap/css/bootstrap.css">
<link type="text/css" rel="stylesheet"href="css/bootstrap/css/bootstrap-theme.css">
<link href="/ecoescambo2016/jquery-mobile/jquery.mobile-1.0.min.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="css/bootstrap/js/bootstrap.js"></script>
<script type="text/javascript" src="css/bootstrap/js/npm.js"></script>
<script src="/ecoescambo2016/jquery-mobile/jquery-1.6.4.min.js" type="text/javascript"></script>
<script src="/ecoescambo2016/jquery-mobile/jquery.mobile-1.0.min.js" type="text/javascript"></script>
</head>

<body>
<div data-role="page" id="page">
  <div data-role="content">
    <div class="container">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <?php require_once("cabecalho.html"); ?>
      </div>
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="col-lg-2 col-md-2 col-sm-1 col-xs-1"></div>
        <div class="col-lg-8 col-md-8 col-sm-10 col-xs-10">
          <div data-role="fieldcontain">
            <fieldset data-role="controlgroup">
              <legend>Para mais informações selecione um item:</legend>
              <a href="bandeija.php">
              <input type="radio" name="catalogo" id="catalogo_0" value="" />
              <label for="catalogo_0">Bandeija</label>
              </a> <a href="faqueiro.php">
              <input type="radio" name="catalogo" id="catalogo_1" value="" />
              <label for="catalogo_1">Faqueiro</label>
              </a> <a href="FerroaSeco.php">
              <input type="radio" name="catalogo" id="catalogo_2" value="" />
              <label for="catalogo_2">Ferro de passar a Seco</label>
              </a> <a href="grill.php">
              <input type="radio" name="catalogo" id="catalogo_3" value="" />
              <label for="catalogo_3">Grill</label>
              </a> <a href="home-theater.php">
              <input type="radio" name="catalogo" id="catalogo_4" value="" />
              <label for="catalogo_4">Home Theater</label>
              </a> <a href="iphone.php">
              <input type="radio" name="catalogo" id="catalogo_5" value="" />
              <label for="catalogo_5">Iphone</label>
              </a> <a hidden="lentes.php">
              <input type="radio" name="catalogo" id="catalogo_6" value="" />
              <label for="catalogo_6">lentes</label>
              </a> <a href="notebook.php">
              <input type="radio" name="catalogo" id="catalogo_7" value="" />
              <label for="catalogo_7">notebook</label>
              </a> <a href="refrigerador.php">
              <input type="radio" name="catalogo" id="catalogo_8" value="" />
              <label for="catalogo_8">refrigerador</label>
              </a> <a href="ventilador.php">
              <input type="radio" name="catalogo" id="catalogo_9" value="" />
              <label for="catalogo_9">ventilador</label>
              </a>
            </fieldset>
          </div>
          <div class="col-lg-3"></div>
          <div data-role="controlgroup" data-type="horizontal"> <a href="login.php" data-role="button" data-icon="check">Entrar</a> <a href="/ecoescambo2016/cadastro_cliente.php" data-role="button" data-icon="check">Registro</a> </div>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-1 col-xs-1"></div>
      </div>
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <?php require_once("rodape.html"); ?>
      </div>
    </div>
  </div>
</div>
</body>
</html>