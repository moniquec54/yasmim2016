<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<link href="css/css.css" rel="stylesheet" type="text/css">
<link type="text/css" rel="stylesheet" href="css/bootstrap/css/bootstrap.css">
<link type="text/css" rel="stylesheet"href="css/bootstrap/css/bootstrap-theme.css">
<script type="text/javascript" src="css/bootstrap/js/bootstrap.js"></script>
<script type="text/javascript" src="css/bootstrap/js/npm.js"></script>
</head>

<body>
<?php
 	 // Recebe um campo de um formulário
	$login = $_POST['login'];
	$senhalogin = $_POST['senhalogin'];

	
/*	$usuarios = array(
		array(
			'id' => 1,
			'nome' => 'belo',
			'usuario' => 'belo',
			'senha' => 'Teste1'
		),
		array(
			'id' => 2,
			'nome' => 'yasmim',
			'usuario' => 'yasmim',
			'senha' => 'Abc123'
		),
		array(
			'id' => 3,
			'nome' => 'teste',
			'usuario' => 'teste',
			'senha' => 'Teste1'
		)

	);*/

	$usuarios = array();
	$usuarios[] = array('nome' => 'belo', 'usuario' => 'belo', 'senha' => 'Teste1');
		
	$mensagem = '';
	$error = false;
	
	if(!empty($_POST)){
		if(empty($_POST['login'])){
			$mensagem .= 'Nome de usu&aacute;rio requisitado.<br><br>';
			$error = $error || true;
		}
		if(!empty($_POST['senhalogin'])){
			if(!(strlen($_POST['senhalogin']) >= 6)){
				$mensagem .= 'Senha curta demais.<br>';
				$error = $error || true;
			}else{
				if((!(preg_match('/[A-Z]/',$_POST['senhalogin']))) || (!(preg_match('/[a-z]/',$_POST['senhalogin']))) || (!(preg_match('/[0-9]/',$_POST['senhalogin'])))){
					$mensagem .= 'Senha precisa ser alfanum&eacute;rica com letras mai&uacute;sculas e min&uacute;sculas.<br>';
					$error = $error || true;
				}
			}
		}else{
			$mensagem .= 'Senha requisitada.<br>';
			$error = $error || true;
		}
		
		if(!$error){
			foreach($usuarios as $usuario){
				if(($usuario['usuario'] == $_POST['login'])&&($usuario['senha'] == $_POST['senhalogin'])){	
					/*session_start();				*/
					$_SESSION['login'] = true;
					$_SESSION['nome'] = $usuario['nome'];
					echo "<script> alert('Logado Com Sucesso!'); </script>";
									
					
				}else{		
					echo "<script> alert('Usuario Não Cadastrado!'); </script>";
					header('Location:index.php');			
					
										
				}
			}
		}
	}
	
	$a = array(
		array(
			'id' => 1,
			'nome_produto' => 'tchururu',
			'imagem' => 'upeloud/produtos/carro.jpg'
		),
		array(
			'id' => 2,
			'nome_produto' => 'tchururu',
			'imagem' => 'upeloud/produtos/carro.jpg'
		),
		array(
			'id' => 3,
			'nome_produto' => 'tchururu',
			'imagem' => 'upeloud/produtos/carro.jpg'
		)
	);
	
?>
<div class="container">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <?php require_once("cabecalho.html"); ?>
  </div>
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="col-lg-2 col-md-2 col-sm-1 col-xs-1"></div>
    <div class="col-lg-8 col-md-8 col-sm-10 col-xs-10">
      <h1> Olá, Bem Vindo,
        <?=strtolower($login)?>
        !</h1>
      <h3> É um sistema para trocas ecológica. </h3>
      <h3>Aqui você se cadastra cadasta é pode traocar produtos com outras pessoas que também estão ofertando produtos para também uma possível troca. </h3>
      <br>
      <?=$mensagem?>
      ; <br>
      <br>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-1 col-xs-1"></div>
  </div>
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <?php require_once("rodape.html"); ?>
  </div>
</div>
</body>
</html>