<?php
 
require_once( "./comum.php");
require_once("./classes/conecao2.php");
require_once("./classes/ClienteDao.php");
require_once("./classes/Cliente.php");


// abre a conexão
 $pdo = new PDO('mysql:host=localhost;dbname=bazar', 'root','');
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  
// pega o ID da URL
$id = isset($_GET['id']) ? $_GET['id'] : null;
 
// valida o ID
if (empty($id))
{
    echo "ID não informado";
    exit;
}

// remove do banco


 
$sql = "DELETE FROM cliente WHERE cod = :id";
$stmt = $pdo->prepare($sql);
//$stmt = Conexao::getInstance()->prepare($sql);
//$stmt = $PDO->prepare($sql);
$stmt->bindParam(':id', $id, PDO::PARAM_INT);
 
if ($stmt->execute())
{
    	header('Location:form_edit2.php');
}
else
{
    echo "Erro ao remover";
    print_r($stmt->errorInfo());
}