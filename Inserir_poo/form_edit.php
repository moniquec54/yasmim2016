<?php
require_once( "./comum.php");
require_once("./classes/conecao2.php");
require_once("./classes/ClienteDao.php");
require_once("./classes/Cliente.php");


// pega o ID da URL
$id = isset($_GET['id']) ? (int) $_GET['id'] : null;

// valida o ID
if (empty($id))
{
	echo "ID para alteração não definido";
	exit;
}

// busca os dados du usuário a ser editado
// abre a conexão
//$PDO = db_connect();
$PDO = new PDO('mysql:host=localhost;dbname=bazar', 'root','');
$PDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$sql = "SELECT cod, nome, sobrenome, senha, telefone, celular, cep, email FROM cliente WHERE cod = :id";
$stmt = $PDO->prepare($sql);
$stmt->bindParam(':id', $id, PDO::PARAM_INT);

$stmt->execute();

$cliente = $stmt->fetch(PDO::FETCH_ASSOC);

// se o método fetch() não retornar um array, significa que o ID não corresponde a um usuário válido
if (!is_array($cliente))
{
	echo "Nenhum usuário encontrado";
	exit;
}
?>
<!doctype html>
<html>
<head>
	<meta charset="utf-8">

	<title>Edição de Usuário - ULTIMATE PHP</title>
</head>

<body>

	<h1>Sistema de Cadastro - ULTIMATE PHP</h1>

	<h2>Edição de Usuário</h2>

	<form action="editar.php" method="post">
		<label for="name">Nome: </label>
		<br>
		<input type="text" name="nome" id="name" value="<?php echo $cliente['nome'] ?>">

		<br><br>

		<label for="email">Sobrenome: </label>
		<br>
		<input type="text" name="sobrenome" id="email" value="<?php echo $cliente['sobrenome'] ?>">

		<br><br>

		<label for="name">Senha: </label>
		<br>
		<input type="text" name="senha" id="name" value="<?php echo $cliente['senha'] ?>">

		<br><br>

		<label for="email">Telefone: </label>
		<br>
		<input type="text" name="telefone" id="email" value="<?php echo $cliente['telefone'] ?>">

		<br><br>


		<label for="name">Celular: </label>
		<br>
		<input type="text" name="celular" id="name" value="<?php echo $cliente['celular'] ?>">

		<br><br>

		<label for="email">Cep: </label>
		<br>
		<input type="text" name="cep" id="email" value="<?php echo $cliente['cep'] ?>">

		<br><br>
		<label for="email">Email: </label>
		<br>		
		<input type="text" name="email" id="email" value="<?php echo $cliente['email'] ?>">
		

		<br><br>

		<input type="hidden" name="id" value="<?php echo $id ?>">

		<input type="submit" value="Alterar">
	</form>

</body>
</html>


<!--







<!DOCTYPE html>
<html>
<head>
	<title>Formulario Editar</title>
</head>
<body>
	<form action="editar.php" method="post"> 
		<label>Codigo do Cliente para alterar: &#160;</label>
		<input type="text" name="codigo" placeholder="Digite o Codigo" ><br><br>

		<label>Novo: &#160;</label>
		<input type="text" name="nome" placeholder="Digite o nome"><br>
		<label>Sobrenome: &#160;</label>
		<input type="text" name="sobrenome" placeholder="Digite o sobrenome"><br>
		<label> Senha: &#160;</label>
		<input type="text" name="senha" placeholder="Digite a senha"><br>
		<label>Telefone: &#160;</label>
		<input type="text" name="telefone" placeholder="Digite o telefone"><br>
		<label>Celular: &#160;</label>
		<input type="text" name="celular" placeholder="Digite o Celular"><br>
		<label>Cep: &#160;</label>
		<input type="text" name="cep" placeholder="Digite o CEP">	<br>
		<label>Email: &#160;</label>
		<input type="text" name="email" placeholder="Digite o email">	<br><br>

		<br><br>
		<button name="inserir" type="submit" class="btn btn-xl">Editar</button>
	</form>
</body>
</html>-->