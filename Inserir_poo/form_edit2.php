<?php
    require_once( "./comum.php");
  require_once("./classes/conecao2.php");
  require_once("./classes/ClienteDao.php");
  require_once("./classes/Cliente.php");

 
// abre a conexão
 $pdo = new PDO('mysql:host=localhost;dbname=bazar', 'root','');
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
 
// SQL para contar o total de registros
// A biblioteca PDO possui o método rowCount(), mas ele pode ser impreciso.
// É recomendável usar a função COUNT da SQL
$sql_count = "SELECT COUNT(*) AS cliente FROM cliente ORDER BY nome ASC";
 
// SQL para selecionar os registros
$sql = "SELECT cod, nome, sobrenome, senha, telefone, celular, cep, email FROM cliente ORDER BY nome ASC";
 
// conta o toal de registros
$stmt_count = $pdo->prepare($sql_count);
$stmt_count->execute();
$total = $stmt_count->fetchColumn();
 
// seleciona os registros
$stmt = $pdo->prepare($sql);
$stmt->execute();
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
 
        <title>Sistema de Cadastro - ULTIMATE PHP</title>
    </head>
 
    <body>
         
        <h1>Sistema de Cadastro - ULTIMATE PHP</h1>
         
        <p><a href="form_inserir.php">Adicionar Usuário</a></p>
 
        <h2>Lista de Usuários</h2>
 
        <p>Total de usuários: <?php echo $total ?></p>
 
        <?php if ($total > 0): ?>
 
        <table width="50%" border="1">
            <thead>
                <tr>
                    <th>Cod</th>
                    <th>Nome</th>
                    <th>Sobrenome</th>
                    <th>Senha</th>
                    <th>Telefone</th>
                    <th>Celular</th>
                    <th>Cep</th>
                    <th>Email</th>

                </tr>
            </thead>
            <tbody>
                <?php while ($cliente = $stmt->fetch(PDO::FETCH_ASSOC)): ?>
                <tr>
                    <td><?php echo $cliente['cod'] ?></td>
                    <td><?php echo $cliente['nome'] ?></td>
                    <td><?php echo $cliente['sobrenome'] ?></td>
                    <td><?php echo $cliente['senha'] ?></td>
                    <td><?php echo $cliente['telefone'] ?></td>
                    <td><?php echo $cliente['celular'] ?></td>                                       
                    <td><?php echo $cliente['cep'] ?></td>
                    <td><?php echo $cliente['email'] ?></td>                    
                    <td>
                        <a href="form_edit.php?id=<?php echo $cliente['cod'] ?>">Editar</a>
                        <a href="delete.php?id=<?php echo $cliente['cod'] ?>" onclick="return confirm('Tem certeza de que deseja remover?');">Remover</a>
                    </td>
                </tr>
                <?php endwhile; ?>
            </tbody>
        </table>
 
        <?php else: ?>
 
        <p>Nenhum usuário registrado</p>
 
        <?php endif; ?>
    </body>
</html>