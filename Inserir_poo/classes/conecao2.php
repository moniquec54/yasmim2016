<?php

class Conexao{

	public static $instance;

	function __construct() {

	}

	public static function getInstance() {
		if (!isset(self::$instance)) {
			try{
				self::$instance = new
				PDO('mysql:host=localhost;dbname=bazar', 'root','', array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
				self::$instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				self::$instance->setAttribute(PDO::ATTR_ORACLE_NULLS, PDO::NULL_EMPTY_STRING);
				//echo "Connected successfully"; 
			}catch(PDOException $e){
				echo "Connection failed: " . $e->getMessage();
			}
		}
		return self::$instance;
	}

}







