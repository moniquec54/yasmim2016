<?php
require_once( "./comum.php");
require_once( BASE_DIR . "/classes/conecao2.php");

class ClienteDao {

    public static $instance;

    function __construct() {


  }

  public static function getInstance() {
    if (!isset(self::$instance))
        self::$instance = new ClienteDao();
    return self::$instance;
}

/*    public function getNextID() {
        try {
            $sql = "SELECT Auto_increment FROM 
            information_schema.tables WHERE table_name='cliente'";
            $result = Conexao::getInstance()->query($sql);
            $final_result = $result->fetch(PDO::FETCH_ASSOC);
            return $final_result['Auto_increment'];
        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar 
            esta ação, foi gerado um LOG do mesmo, tente novamente mais 
            tarde.";
        }
    }
*/
    public function inserir(Cliente $cliente) {
        try {
            $sql = "INSERT INTO cliente (nome, sobrenome, senha, telefone, celular, cep, email)
            VALUES (
            :nome,
            :sobrenome,
            :senha,
            :telefone,
            :celular,
            :cep,
            :email)";

            //$stmt =  $con = new PDO("mysql:host=localhost;dbname=nomedobanco", "root", "senhadobanco");
//            $p_sql = Conexao::getInstance()->prepare();
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":nome", $cliente->getNome());
            $p_sql->bindValue(":sobrenome", $cliente->getSobrenome());
            $p_sql->bindValue(":senha", $cliente->getSenha());            
            $p_sql->bindValue(":telefone", $cliente->getTelefone());
            $p_sql->bindValue(":celular", $cliente->getCelular());
            $p_sql->bindValue(":cep", $cliente->getCep());
            $p_sql->bindValue(":email", $cliente->getEmail());
            echo "Cadastro realizado com Sucesso: " ;
            return $p_sql->execute();
//            header('location:sucesso.php');

        } catch (Exception $e) {
            print "Ocorreu um erro ao tentar executar esta ação de inserir, foi gerado um LOG do mesmo, tente novamente mais tarde.";
        }
    }



    /* consultar*/
    public static function rowMapper($cod, $nome, $sobrenome, $senha, $telefone, $celular, $cep, $email)
    {
        return new Cliente( $cod, $nome, $sobrenome, $senha, $telefone, $celular, $cep, $email);
    }

    public static function findAll()
    {
      //$pdo = Banco::obterConexao();
        $sql = "SELECT nome, sobrenome, senha, telefone, celular, cep, email FROM cliente";
        $p_sql = Conexao::getInstance()->prepare($sql);

      /*
      return $statement->fetchAll( PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE,
      "Categoria", array( 'xxxx', 'xxx', 'xxx') );
      */
      return $p_sql->execute();      
  }


}
