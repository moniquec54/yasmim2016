<?php

//classe modelo
class Cliente {

    private $cod;
    private $nome;
    private $sobrenome;
    private $senha;
    private $telefone;
    private $celular;
    private $cep;
    private $email;
    

    function __construct() {

    }

/* inserir*/
    function getCod() {
        return $this->cod;
    }

    function getNome() {
        return $this->nome;
    }
    function getSobrenome(){
        return $this->sobrenome;
    }
    function getSenha(){
        return $this->senha;
    }
     function getTelefone() {
        return $this->telefone;
    }
    function getCelular(){
        return $this->celular;
    }
    function getCep(){
        return $this->cep;
    }

    function getEmail() {
        return $this->email;
    }

   
/*ler*/

    function setCod($cod) {
        $this->cod = $cod;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }
    function setSobrenome($sobrenome){
        $this->sobrenome = $sobrenome;
    }
    function setSenha($senha){
        $this->senha = $senha;
    }
    function setTelefone($telefone) {
        $this->telefone = $telefone;
    }
    function setCelular($celular){
        $this->celular = $celular;
    }
    function setCep($cep){
        $this->cep = $cep;
    }

    function setEmail($email) {
        $this->email = $email;
    }

  
}