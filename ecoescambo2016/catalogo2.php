<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Cadastro Produto</title>
<?php require_once("link.php");?>
<!--Link CSS e JS -->
</head>

<body>
<div data-role="page" id="cadastro_produto">
  <div data-role="content">
    <div class="container">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <?php require_once("cabecalho.html"); ?>
      </div>
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="col-lg-2 col-md-2 col-sm-1 col-xs-1"></div>
        <div class="col-lg-8 col-md-8 col-sm-10 col-xs-10">
          <h1>Catalogo de Produtos</h1>
          <form action="Adicionar_produto.php" method="get" accept-charset="UTF-8">
            <div data-role="fieldcontain">
              <fieldset data-role="controlgroup">
                <legend>Para mais informações selecione um item:</legend>
                <input type="radio" name="catalogo1" id="catalogo1_0" value="" onClick="window.location = 'bandeija.php';"/>
                <label for="catalogo1_0">Bandeija</label>
                <input type="radio" name="catalogo1" id="catalogo1_1" value="" onClick="window.location = 'faqueiro.php';"/>
                <label for="catalogo1_1">Faqueiro</label>
                <input type="radio" name="catalogo1" id="catalogo1_2" value="" onClick="window.location = 'FerroaSeco.php';"/>
                <label for="catalogo1_2">Ferro de Passar Roupa </label>
                <input type="radio" name="catalogo1" id="catalogo1_3" value="" onClick="window.location = 'home-theater.php';"/>
                <label for="catalogo1_3">Home Theater</label>
                <input type="radio" name="catalogo1" id="catalogo1_4" value="" onClick="window.location = 'iphone.php';"/>
                <label for="catalogo1_4">iphone</label>
                <input type="radio" name="catalogo1" id="catalogo1_5" value="" onClick="window.location = 'lentes.php';"/>
                <label for="catalogo1_5">Kits de lentes</label>
                <input type="radio" name="catalogo1" id="catalogo1_6" value="" onClick="window.location = 'notebook.php';"/>
                <label for="catalogo1_6">Notebook</label>
                <input type="radio" name="catalogo1" id="catalogo1_7" value="" onClick="window.location = 'refrigerador.php';"/>
                <label for="catalogo1_7">Refrigerador</label>
                <input type="radio" name="catalogo1" id="catalogo1_8" value="" onClick="window.location = 'ventilador.php';"/>
                <label for="catalogo1_8">Ventilador</label>
                <input type="radio" name="catalogo1" id="catalogo1_9" value="" onClick="window.location = 'grill.php';"/>
                <label for="catalogo1_9">Grill</label>
              </fieldset>
            </div>
          </form>
        </div>
      </div>
      <div class="col-lg-2 col-md-2 col-sm-1 col-xs-1"></div>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <?php require_once("rodape.html"); ?>
    </div>
  </div>
</div>
</div>
</body>
</html>