﻿<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Cadastro Produto</title>
<?php require_once("link.php");?>
<!--Link CSS e JS -->
</head>

<body>
<div data-role="page" id="cadastro_produto">
  <div data-role="content">
    <div class="container">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <?php require_once("cabecalho.html"); ?>
      </div>
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="col-lg-2 col-md-2 col-sm-1 col-xs-1"></div>
        <div class="col-lg-8 col-md-8 col-sm-10 col-xs-10">
          <h1>iPhone 5S Apple 16GB</h1>
          <form action="" method="get" accept-charset="UTF-8">
            <div data-role="fieldcontain">
              <label for="textinput">Nome Produto:</label>
              <input type="text" name="nome_produto" id="textinput" readonly value="iPhone 5S Apple 16GB" minlength="3" maxlength="45" />
            </div>
            <div data-role="fieldcontain">
              <label for="textinput">Descrição Produto:</label>
              <textarea  class="ui-input-text ui-body-c ui-corner-all ui-shadow-inset" name="descricao_produto" readonly>A Tramontina existe para desenvolver, produzir e entregar produtos de qualidade, que tornem melhor a vida das pessoas. Beleza e funcionalidade! A linha de faqueiros Laguna, da Tramontina, possui peças em aço inoxidável, altamente duráveis, preservando a beleza, higiene e durabilidade do material.

Ideal para usar quando for receber visitas, combina com qualquer ambiente. O polimento em alto brilho com detalhes em baixo relevo no cabo valoriza as peças.</textarea>
            </div>
            <div data-role="fieldcontain">
              <label for="textinput"> Foto:</label>
              <img class="fotos-produtos" src="imagem/produtos/iphone.jpg" > 
              
              <!--   Para controlar o upload no PHP, é bem simples. Os arquivos serão enviados como um array, neste caso, você terá que fazer um foreach e um upload individual para cada arquivo. Observem o código abaixo.

 <input type="file" data-clear-btn="true" name="foto_produto[]" multiple >
 
 
   # Declarar a variável "i", que será nosso controle, para que não haja
   # loop infinito.
   $i = 0;
  
   # Faz um loop conforme o número de arquivos
   foreach ($_FILES["uploads"]["error"] as $key => $error) {
  
     # Definir a pasta que os arquivos serão "upados".
     $pasta = "fotos/_" . $_FILES["uploads"]["name"][$i];
  
     # Aqui, você faz o upload do arquivo, utilizando a classe que você
     # tem aí.
     $ftp->upload($_FILES["uploads"]["tmp_name"][$i], $pasta);
  
     # Agora o arquivo já foi upado, pode fazer alguns scripts adicionais, como por exemplo
     # adicionar o nome dele no banco de dados, ou talvez alertar o nome de cada arquivo.
  
     # Incrementar algum um valor a mais na variável "i" para que não ocorra loop infinito.
     ++$i;
   }
      --> 
            </div>
            <div class="col-lg-3"></div>
            <div data-role="controlgroup" data-type="horizontal" class="col-lg-6"> <br>
              <button type="submit" data-icon="check" >Enviar</button>
              <button type="reset" data-icon="arrow-l" onClick="history.go(-1)">Voltar</button>
            </div>
          </form>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-1 col-xs-1"></div>
      </div>
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <?php require_once("rodape.html"); ?>
      </div>
    </div>
  </div>
</div>
</body>
</html>