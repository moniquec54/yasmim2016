<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Cadastro Cliente</title>
<link href="jquery-mobile/SpryValidationConfirm.css" rel="stylesheet" type="text/css">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="jquery-mobile/jquery-1.8.3.min.js" type="text/javascript"></script>
<script src="jquery-mobile/jquery.mobile-1.3.0.min.js" type="text/javascript"></script>
<link href="css/css.css" rel="stylesheet" type="text/css">
<link href="jquery-mobile/jquery.mobile-1.3.0.min.css" rel="stylesheet" type="text/css">

<link type="text/css" rel="stylesheet" href="css/bootstrap/css/bootstrap.min.css">
<link type="text/css" rel="stylesheet"href="css/bootstrap/css/bootstrap-theme.min.css">
<script type="text/javascript" src="css/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="css/bootstrap/js/npm.js"></script>
<script type="text/javascript" src="jquery/jquery-3.1.1.js"></script>

<!-- Importação O <canvas>elemento é um elemento novo para além do HTML5. Ele permite que você desenhar formas, caminhos, imagens e outros desenhos em um elemento em branco chamado a tela. -->

<script type="text/javascript" src="jquery/jcanvas.min.js"></script>

<!--mascara tel, cep, cpf, etc -->
<script type="text/javascript" src="jquery/jquery.mask.js"/></script>


<script src="jquery-mobile/SpryValidationConfirm.js" type="text/javascript"></script>
</head>
<body>
<div data-role="page" id="cadastro_cliente_mobile">
  <div data-role="content">
    <div class="container">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <?php require_once("cabecalho.html"); ?>
      </div>
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="col-lg-2 col-md-2 col-sm-1 col-xs-1"></div>
        <div class="col-lg-8 col-md-8 col-sm-10 col-xs-10">
          <h1> Cadastro de Cliente</h1>
          <form action="" method="post" name="from_cadastrar_cliente" accept-charset="UTF-8" >
            <div data-role="fieldcontain">
              <label for="textinput">Nome:</label>
              <input name="nome_cliente"  type="text" autofocus required id="textinput"  data-clear-btn="true" placeholder="Digite o nome" tabindex="1" value="" maxlength="25"  minlength="3" />
            </div>
            <div data-role="fieldcontain">
              <label for="textinput2">Sobrenome:</label>
              <input name="sobrenome_cliente" data-clear-btn="true" type="text" required id="textinput2" placeholder="Digite o sobrenome" tabindex="2" value="" autocomplete="off" maxlength="45" minlength="3"/>
            </div>
            <div data-role="fieldcontain">
              <label for="textinput3">Senha:</label>
              <input name="senha_cliente" data-clear-btn="true" type="password" autocomplete="off"  required id="textinput3"pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}" title="Mínimo 6 caracteres, Contendo pelo menos 1 número, 1 maiúscula e 1 minúscula" tabindex="3" value="" maxlength="45" minlength="6" placeholder="Digite a senha"/>
            </div>
            <div data-role="fieldcontain"> <span id="spryconfirm1">Confirmar de senha:&#160;&#160;&#160;
              <input type="password" name="confirmarsenha" placeholder="Digite novamente sua senha" id="confirmarsenha" required>
              <span class="confirmRequiredMsg">Confirme sua senha.</span><span class="confirmInvalidMsg">As senhas não conferem!</span></span> </div>
            <div data-role="fieldcontain">
              <label for="textinput3">Telefone:</label>
              <input type="tel" name="telefone_cliente" id="idtelefone" data-clear-btn="true" tabindex="5" value=""  placeholder="(00)0000-0000" pattern="\([0-9]{2}\)[\s][0-9]{4}-[0-9]{4,5}" required title="Somente numeros"/>
              <!-- Inicio configuração mascara telefone--> 
              <script type="text/javascript">$("#idtelefone").mask("(00) 0000-0000");</script> 
              <!-- Fim configuração mascara telefone--> 
            </div>
            <div data-role="fieldcontain">
              <label for="textinput3">Celular:</label>
              <input type="text" name="celular_cliente" id="idcelular" tabindex="6" value="" data-clear-btn="true" placeholder="(00)00000-0000" pattern="\([0-9]{2}\)[\s][0-9]{4,5}-[0-9]{4}" title="Somente numeros"/>
              <!-- Inicio configuração mascara telefone--> 
              <script type="text/javascript">$("#idcelular").mask("(00) 90000-0000");</script> 
              <!-- Fim configuração mascara telefone--> 
            </div>
            <div data-role="fieldcontain">
              <label for="textinput3">Cep:</label>
              <input type="text" name="cep_cliente" id="idcep" tabindex="7" value="" placeholder="00000-000" required pattern="00000-000" title="Somente numeros"/>
              <!-- Inicio configuração mascara telefone--> 
              <script type="text/javascript">$("#idcep").mask("00000-000");</script> 
              <!-- Fim configuração mascara telefone--> 
            </div>
            <label for="textinput4"></label>
            <div data-role="fieldcontain">
              <label for="textinput4">Email:</label>
              <input type="email" data-clear-btn="true" name="email_cliente" tabindex="7" id="textinput4" value=""  minlength="8" data-clear-icon="true" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="Exemplo: nome@servidor.com" placeholder="Digite seu email"/>
            </div>
            <div class="col-lg-3"></div>
            <div data-role="controlgroup" data-type="horizontal"  class="col-lg-6" > <br>
              <button type="submit" data-icon="check" tabindex="9" >Enviar</button>
              <button type="reset" data-icon="refresh" tabindex="10">Limpar</button>
              <script>
			  	function validarsenha(){
					senha_cliente = from_cadastrar_cliente.senha_cliente.value;
					confirmar_senha_cliente=from_cadastrar_cliente.confirmar_senha_cliente;
					
					if(senha_cliente != confirmar_senha_cliente)
					alert("As senhas não conferem! \n \n Digite novamente a senha \n\n E valide a sua senha em confirmar senha.");
					
				}
			  </script> 
            </div>
          </form>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-1 col-xs-1"></div>
      </div>
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <?php require_once("rodape.html"); ?>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
var spryconfirm1 = new Spry.Widget.ValidationConfirm("spryconfirm1", "textinput3", {validateOn:["blur", "change"]});
</script>
</body>
</html>