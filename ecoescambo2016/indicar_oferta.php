<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Indicar oferta</title>
<?php require_once("link.php");?>
<!--Link CSS e JS -->
</head>

<body>
<div data-role="page" id="cadastro_produto">
  <div data-role="content">
    <div class="container">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <?php require_once("cabecalho.html"); ?>
      </div>
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="col-lg-2 col-md-2 col-sm-1 col-xs-1"></div>
        <div class="col-lg-8 col-md-8 col-sm-10 col-xs-10">
          <h1>Indicar Oferta</h1>
          <h3> Olá, tenho interese no seus produtos: </h3>
          <h3> Estou enviando minhas ofertas, para você avaliar e vé se aceita a troca!</h3>
          <form action="" method="post" accept-charset="UTF-8">
            <div data-role="fieldcontain">
              <fieldset data-role="controlgroup">
                <legend>Option</legend>
                <input type="checkbox" name="varrerproduto" id="varrerproduto_0" class="custom" value="" />
                <label for="varrerproduto_0">Bandeija</label>
                <input type="checkbox" name="varrerproduto" id="varrerproduto_1" class="custom" value="" />
                <label for="varrerproduto_1">Faqueiro</label>
                <input type="checkbox" name="varrerproduto" id="varrerproduto_2" class="custom" value="" />
                <label for="varrerproduto_2">Ferro de Passar Roupa</label>
                <input type="checkbox" name="varrerproduto" id="varrerproduto_3" class="custom" value="" />
                <label for="varrerproduto_3">Home Theater</label>
                <input type="checkbox" name="varrerproduto" id="varrerproduto_4" class="custom" value="" />
                <label for="varrerproduto_4">Kit de Lentes</label>
                <input type="checkbox" name="varrerproduto" id="varrerproduto_5" class="custom" value="" />
                <label for="varrerproduto_5">Notebook</label>
                <input type="checkbox" name="varrerproduto" id="varrerproduto_6" class="custom" value="" />
                <label for="varrerproduto_6">Refrigerador</label>
                <input type="checkbox" name="varrerproduto" id="varrerproduto_7" class="custom" value="" />
                <label for="varrerproduto_7">Ventilador</label>
                <input type="checkbox" name="varrerproduto" id="varrerproduto_8" class="custom" value="" />
                <label for="varrerproduto_8">Grill</label>
                <input type="checkbox" name="varrerproduto" id="varrerproduto_9" class="custom" value="" />
                <label for="varrerproduto_9">Iphone</label>
              </fieldset>
            </div>
            <div class="col-lg-3"></div>
            <div data-role="controlgroup" data-type="horizontal" class="col-lg-7"> <br>
              <button type="submit" data-icon="check" >Aceito</button>
              <button type="reset" data-icon="refresh">Limpar</button>
              <button type="reset" data-icon="alert">Não desejo</button>
            </div>
          </form>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-1 col-xs-1"></div>
      </div>
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <?php require_once("rodape.html"); ?>
      </div>
    </div>
  </div>
</div>
</body>
</html>