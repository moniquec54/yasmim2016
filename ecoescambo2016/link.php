<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="jquery-mobile/jquery-1.8.3.min.js" type="text/javascript"></script>
<script src="jquery-mobile/jquery.mobile-1.3.0.min.js" type="text/javascript"></script>
<link href="css/css.css" rel="stylesheet" type="text/css">
<link href="jquery-mobile/jquery.mobile-1.3.0.min.css" rel="stylesheet" type="text/css">
<link type="text/css" rel="stylesheet" href="css/bootstrap/css/bootstrap.min.css">
<link type="text/css" rel="stylesheet"href="css/bootstrap/css/bootstrap-theme.min.css">
<script type="text/javascript" src="css/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="css/bootstrap/js/npm.js"></script>
<script type="text/javascript" src="jquery/jquery-3.1.1.js"></script>

<!-- Importação O <canvas>elemento é um elemento novo para além do HTML5. Ele permite que você desenhar formas, caminhos, imagens e outros desenhos em um elemento em branco chamado a tela. -->

<script type="text/javascript" src="jquery/jcanvas.min.js"></script>

<!--mascara tel, cep, cpf, etc -->
<script type="text/javascript" src="jquery/jquery.mask.js"/></script>

<!--?php require_once("sessao.php");?><!--importa sessao para todo o programa -->
