<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Login</title>
<?php require_once("link.php");?>
<!--Link CSS e JS -->

</head>
<body>
<div data-role="page" id="page-mobile">
  <div data-role="content">
    <div class="container">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <?php require_once("cabecalho.html"); ?>
      </div>
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="col-lg-2 col-md-2 col-sm-1 col-xs-1"></div>
        <div class="col-lg-8 col-md-8 col-sm-10 col-xs-10">
          <h1>Fazer login </h1>
          <form action="areaprincipal.php" method="post" autocomplete="on" accept-charset="UTF-8" >
            <div data-role="fieldcontain">
              <label for="textinput">Login:</label>
              <input name="login" type="text" autofocus required id="textinput" data-clear-btn="true" tabindex="1" placeholder="digite seu login"  autocomplete="on" value="" data-clear-icon="true" title="belo"/>
              
              <!--input name="login" type="email" autofocus required id="textinput" data-clear-btn="true" tabindex="1" placeholder="digite seu email"  autocomplete="on" value="" data-clear-icon="true" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="Exemplo: nome@servidor.com"/--> 
            </div>
            <div data-role="fieldcontain">
              <label for="passwordinput">Senha:</label>
              <input type="password" name="senhalogin" placeholder="Digite sua Senha: Teste1" id="passwordinput" data-clear-btn="true" value="" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}" title="Mínimo 6 caracteres, Contendo pelo menos 1 número, 1 maiúscula e 1 minúscula. Senha:Teste1"/>
              <!--type = "password" que deve conter 8 ou mais caracteres que são de pelo menos um número e uma maiúscula e letra minúscula:--> 
            </div>
            <h4><a href="cadastro_cliente.php" tabindex="3">Criar uma conta</a></h4>
            <div class="col-lg-3"></div>
            <div data-role="controlgroup" data-type="horizontal" class="col-lg-6"> <br>
              <button type="submit" tabindex="4" data-icon="arrow-r" >Login</button>
              <!--a href="areaprincipal.php" data-icon="arrow-r" data-role="button" type="submit">Login</a-->
              <button type="reset" tabindex="5" data-icon="refresh">Limpar</button>
              <br>
              <br>
              <br>
              <br>
            </div>
          </form>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-1 col-xs-1"></div>
      </div>
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <?php require_once("rodape.html"); ?>
      </div>
    </div>
  </div>
</div>
</body>
</html>