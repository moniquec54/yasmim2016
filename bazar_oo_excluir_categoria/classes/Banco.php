<?php
abstract class Banco
{
  /**
  * Retorne um objeto PDO conectado
  */
  public static function obterConexao()
  {
    $pdo = new PDO('mysql:host=localhost;dbname=bazar;charset=utf8mb4', 'root', 'vertrigo');
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $pdo;
  }
}
